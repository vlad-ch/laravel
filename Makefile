init-laradock: laradock-add-submodule laradock-cp-env

laradock-add-submodule:
	git submodule add git@gitlab.com:vlad-ch/laradock.git

laradock-cp-env:
	(cd laradock && cp .env.example .env)

init-laravel: perm cp-env-laravel build composer-update assets-install key-generate migrate

up:
	(cd laradock && docker-compose up -d nginx php-worker postgres redis workspace)

down:
	(cd laradock && docker-compose down)

build:
	(cd laradock && docker-compose up -d --build nginx php-worker postgres redis workspace)

test:
	(cd laradock && docker-compose exec --user=laradock workspace vendor/bin/phpunit)

workspace:
	(cd laradock && docker-compose exec --user=laradock workspace bash)

in: up workspace

cp-env-laravel:
	cp .env.example .env

key-generate:
	(cd laradock && docker-compose exec --user=laradock workspace php artisan key\:generate)

migrate:
	(cd laradock && docker-compose exec --user=laradock workspace php artisan migrate)

composer-install:
	(cd laradock && docker-compose exec --user=laradock workspace composer install)

composer-update:
	(cd laradock && docker-compose exec --user=laradock workspace composer update)

assets-install:
	(cd laradock && docker-compose exec --user=laradock workspace yarn install)

assets-rebuild:
	(cd laradock && docker-compose exec --user=laradock workspace npm rebuild node-sass --force)

assets-prod:
	(cd laradock && docker-compose exec --user=laradock workspace yarn run prod)

assets-dev:
	(cd laradock && docker-compose exec --user=laradock workspace yarn run dev)

assets-watch:
	(cd laradock && docker-compose exec --user=laradock workspace yarn run watch)

supervisor-refresh:
	(cd laradock && docker-compose exec php-worker supervisorctl reread)
	(cd laradock && docker-compose exec php-worker supervisorctl update)
	(cd laradock && docker-compose exec php-worker supervisorctl start all)

memory:
	sudo sysctl -w vm.max_map_count=262144

perm:
	#sudo chown $USER:$USER -R storage/
	sudo chgrp -R www-data storage bootstrap/cache
	sudo chmod -R ug+rwx storage bootstrap/cache
